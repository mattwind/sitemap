#!/bin/bash

WEBSITE="canara.com"
PROTOCOL="https"
DATE=$(date +"%Y%m%d")
XML_FILENAME=sitemap-$DATE.xml

# download links to tmp file
wget --wait 1 \
     --spider \
     --recursive \
     --level=1 \
     --no-verbose \
     --output-file=spytor.log \
     --header="Accept: text/html" \
     --user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0" \
     --reject=.rss,.gif,.png,.jpg,.css,.js,.txt \
     -e robots=off $PROTOCOL://$WEBSITE;

# parse for urls
urls=(`sed -n 's/.\+ URL:\([^ ]\+\) .\+/\1/p' spytor.log`);

# build xml header
echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<urlset
  xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"
  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
  xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9
  http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">" > $XML_FILENAME;

# add urls to xml sitemap
for url in ${urls[*]}
do
  url=$(echo $url | tr -d '"')
  if [ "$url" != '[1]' ];
  then
    echo -e "  <url>\n    <loc>$url</loc>\n    <changefreq>daily</changefreq>\n  </url>" >> $XML_FILENAME;
  fi
done      
        
echo "</urlset>" >> $XML_FILENAME;

# clean up
rm -rf spytor.log;
rm -rf $WEBSITE

# done
echo "All done!"
echo "Your sitemap can be found here, $(pwd)/$XML_FILENAME"
echo
